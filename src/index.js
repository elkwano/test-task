import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './containers/App/index';
import store from './store/store';

store.subscribe(() => {
  localStorage.setItem('reduxState', JSON.stringify(store.getState()))
});

render(
  <Provider store={store}>
    <App/>
  </Provider>, document.getElementById('root')
);