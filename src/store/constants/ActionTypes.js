export const GET_ALL_CURRENCIES = 'GET_ALL_CURRENCIES';
export const GET_ALL_CURRENCIES_SUCCESS = 'GET_ALL_CURRENCIES_SUCCESS';
export const GET_ALL_CURRENCIES_FAILURE = 'GET_ALL_CURRENCIES_FAILURE';
export const GET_CURRENCIES_RATES = 'GET_CURRENCIES_RATES';
export const GET_CURRENCIES_RATES_SUCCESS = 'GET_CURRENCIES_RATES_SUCCESS';
export const GET_CURRENCIES_RATES_FAILURE = 'GET_CURRENCIES_RATES_FAILURE';
export const GET_FILTERED_CURRENCY_CODES = 'GET_FILTERED_CURRENCY_CODES';
export const ADD_CURRENCY = 'ADD_CURRENCY';
export const REMOVE_CURRENCY = 'REMOVE_CURRENCY';