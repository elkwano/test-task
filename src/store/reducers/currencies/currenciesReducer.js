import * as types from "../../constants/ActionTypes";
import _ from 'lodash';

const defaultState = {
  isLoading: false,
  rates: {},
  codes: [],
  show: ['RUB', 'EUR'],
  base: 'USD'
};

export default function allCurrenciesReducer(state = defaultState, action = {}) {
  switch (action.type) {
    case types.GET_ALL_CURRENCIES:
      return {
        ...state,
        isLoading: true,
      };

    case types.GET_ALL_CURRENCIES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        errorMsg: null,
        codes: _.without(Object.keys(action.payload).filter(item => !state.show.includes(item)), state.base)
      };

    case types.GET_ALL_CURRENCIES_FAILURE:
      return {
        ...state,
        isLoading: false,
        errorMsg: action.payload
      };

    case types.GET_FILTERED_CURRENCY_CODES:
      return {
        ...state,
        codes: _.without(state.codes.filter(item => !state.show.includes(item)), state.base)
      };

    case types.GET_CURRENCIES_RATES:
      return {
        ...state,
        isLoading: true
      };

    case types.GET_CURRENCIES_RATES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        errorMsg: null,
        rates: action.payload.rates
      };

    case types.GET_CURRENCIES_RATES_FAILURE:
      return {
        ...state,
        isLoading: false,
        errorMsg: action.payload
      };

    case types.REMOVE_CURRENCY:
      return {
        ...state,
        show: _.without(state.show, action.payload)
      };

    case types.ADD_CURRENCY:
      return {
        ...state,
        state: state.show.push(action.payload)
      };

    default:
      return state;
  }
}