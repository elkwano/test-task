import { combineReducers } from 'redux';
import currencies from './currencies/currenciesReducer'

export default combineReducers({
  currencies
});
