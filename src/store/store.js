import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers/rootReducer';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

const persistedState = localStorage.getItem('reduxState') ? JSON.parse(localStorage.getItem('reduxState')) : {};
const middleware = applyMiddleware(thunk, createLogger());
const store = createStore(rootReducer, persistedState, middleware);

export default store;