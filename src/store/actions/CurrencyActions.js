import * as types from "../constants/ActionTypes";
import axios from "axios/index";

const APP_ID = '25f9cace00c946fab4d8d8bb09907854';

export const getCurrencyCodes = () => {
  return (dispatch, getState) => {
    dispatch({type: types.GET_ALL_CURRENCIES});

    return axios.get('https://openexchangerates.org/api/currencies.json')
        .then(res => {
          dispatch({type: types.GET_ALL_CURRENCIES_SUCCESS, payload: res.data});
        })
        .catch(error => {
          dispatch({type: types.GET_ALL_CURRENCIES_FAILURE, payload: error.response ? error.response.data.error : error});
        })
  }
};

export const getFilteredCurrencyCodes = () => {
  return (dispatch, getState) => {
    return dispatch({ type: types.GET_FILTERED_CURRENCY_CODES });
  }
};

export const getRates = () => {
  return (dispatch, getState) => {
    const willBeShown = getState().currencies.show.join();

    if (willBeShown.length === 0) {
      return dispatch({ type: types.GET_CURRENCIES_RATES_SUCCESS, payload: {rates: {}} })
    }

    dispatch({type: types.GET_CURRENCIES_RATES});

    return axios.get('https://openexchangerates.org/api/latest.json', { params: { app_id: APP_ID, symbols: willBeShown }})
      .then(res => {
        dispatch({ type: types.GET_CURRENCIES_RATES_SUCCESS, payload: res.data })
      })
      .catch(error => {
        dispatch({ type: types.GET_CURRENCIES_RATES_FAILURE, payload: error.response ? error.response.data.error : error })
      })
  }
};

export const addCurrency = name => {
  return (dispatch, getState) => {
    dispatch({ type: types.ADD_CURRENCY, payload: name });
    dispatch(getRates());

    return Promise.resolve()
  }
};

export const removeCurrency = name => {
  return (dispatch, getState) => {
    dispatch({ type: types.REMOVE_CURRENCY, payload: name });
    dispatch(getRates());

    return Promise.resolve();
  }
};