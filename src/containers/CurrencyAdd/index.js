import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import { addCurrency, getFilteredCurrencyCodes } from "../../store/actions/CurrencyActions";
import './styles.scss'

class CurrencyAdd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      codes: props.codes,
      show: props.show,
      currencyToBeAdd: ''
    };

    this.select = React.createRef();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.codes !== prevState.codes) {
      return {
        codes: nextProps.codes
      }
    }

    if (nextProps.show !== nextProps.show) {
      return {
        show: nextProps.show
      }
    }

    return null;
  }

  renderOptions = codes => {
    return codes.map(item => {
      return <option key={item} value={item}>{ item }</option>
    })
  };

  add = name => {
    this.props.addCurrency(name).then(() => {
      this.props.getFilteredCurrencyCodes();
      this.setState({
        currencyToBeAdd: ''
      });
    });
  };

  handleChange = () => event => {
    this.setState({
      currencyToBeAdd: event.target.value
    })
  };

  render() {
    const { codes, currencyToBeAdd } = this.state;
    const options = this.renderOptions(codes);

    return (
        <div className="add-currency-wrapper">
          <select onChange={this.handleChange()} value={currencyToBeAdd} ref={this.select}>
            <option value=""/>
            { options }
          </select>
          <button
              type="button"
              className="add-button"
              onClick={() => this.add(currencyToBeAdd)}>
            <i className="icon">add</i>
          </button>
        </div>

    )
  }
}

function mapStateToProps(state) {
  return state.currencies;
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({ addCurrency, getFilteredCurrencyCodes }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CurrencyAdd);