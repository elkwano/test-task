import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getCurrencyCodes, getRates } from '../../store/actions/CurrencyActions'
import CurrencyAdd from "../../containers/CurrencyAdd";
import CurrencyList from "../../containers/CurrencyList";
import './styles.scss'

class Index extends Component {
  constructor(props) {
    super(props);
  }

  update = () => {
    this.props.getCurrencyCodes();
    this.props.getRates();
  };

  componentDidMount () {
    this.update();
  }

  render () {
    return (
        <div className="exchange-rates">
          <div className="content">
            <header>
              <h4>Exchange rates</h4>
              <CurrencyAdd />
            </header>
            <CurrencyList/>
          </div>
        </div>
    )
  }
}

function mapStateToProps(state) {
  return state.currencies;
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({getCurrencyCodes, getRates}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)