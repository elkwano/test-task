import React, { Component } from 'react';
import { connect } from 'react-redux'
import {bindActionCreators} from "redux";
import {removeCurrency} from "../store/actions/CurrencyActions";

class CurrencyRemove extends Component {
  constructor(props) {
    super(props);
  }

  remove = () => {
    this.props.removeCurrency(this.props.id);
  };

  render() {
    return (
      <button
          type="button"
          className="delete-button"
          onClick={() => this.remove()}>
        <i className="icon">delete</i>
      </button>
    )
  }
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({removeCurrency}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CurrencyRemove)