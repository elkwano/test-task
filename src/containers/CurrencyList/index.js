import React, { Component } from 'react'
import { connect } from 'react-redux'
import CurrencyRemove from '../../containers/CurrencyRemove'
import './styles.scss'

class CurrencyList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      rates: this.props.rates,
      tableHeadings: ['currency', 'rate']
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.rates !== prevState.rates) {
      return {
        rates: nextProps.rates
      }
    }

    return null;
  }

  renderTableHeader = () => {
    const { tableHeadings } = this.state;
    return (
        <thead>
          <tr>
            {tableHeadings.map(item => <th key={item}>{ item }</th>)}
          </tr>
        </thead>
    )
  };

  renderTableBody = () => {
    const { rates } = this.state;

    if (!Object.keys(rates).length) {
      return (
          <tbody>
            <tr>
              <td colSpan="2">Sorry, there are nothing to render :(</td>
            </tr>
          </tbody>
      )
    }

    return (
        <tbody>
        {Object.keys(rates).map(item => (
            <tr key={item}>
              <td>{item}</td>
              <td>
                <div className="cell-wrapper">
                  <div className="value">{rates[item]}</div>
                  <CurrencyRemove id={item}/>
                </div>
              </td>
            </tr>
        ))}
        </tbody>
    )
  };

  render() {
    const tableHeader = this.renderTableHeader();
    const tableBody = this.renderTableBody();

    return (
        <div className="table-wrapper">
          <table className="list-table">
            { tableHeader }
            { tableBody }
          </table>
        </div>
    )
  }
}

function mapStateToProps(state) {
  return state.currencies;
}

export default connect(mapStateToProps, {})(CurrencyList)